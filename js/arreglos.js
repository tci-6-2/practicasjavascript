/*
// Declaración de arreglos
var arreglo = [19, 20, 3, 2, 6, 3, 3, 9];
// Mostrar los elementos de arreglos
function mostrarArreglo() {
    for (con = 0; con < arreglo.length; ++con) {
        console.log(con + ":" + arreglo[con]);
    }
}
*/
/*
// Función que regrese el promedio de los elementos del arreglo
function promArreglo() {
    let prom = 0;
    for (con = 0; con < arreglo.length; ++con) {
        prom = prom + arreglo[con];
    }
    prom = prom / arreglo.length;
    return prom;
}
*/

/* Realizar una función que regrese un valor entero que 
represente la cantidad de números pares en el arreglo */
/*
function numPar() {
    let par = 0;
    for (con = 0; con < arreglo.length; ++con) {
        if (arreglo[con] % 2 == 0) {
            par++;
        } else {
            console.log("no es par");
        }
    }
    return par;
}
*/

/*
// Realizar una función que te regrese el valor mayor contenido en el arreglo
function numMayor() {
    let mayor = 0;
    for (con = 0; con < arreglo.length; ++con) {

        if (mayor <= arreglo[con]) {
            mayor = arreglo[con];
        } else {
            console.log("No es mayor que el anterior");
        }
    }
    return mayor;
}
*/

/*
// Llamamos la función para mostrar el arreglo
mostrarArreglo();
// Llamamos la función para mostrar el promedio del arreglo
const prom = promArreglo();
console.log("El Promedio del Arreglo es: " + prom);
// Llamamos la funcion paara los numero pares
const par = numPar();
console.log("EL total de números pares es: " + par);
// Llamamos la funcion para el número mayor
const mayor = numMayor();
console.log("El Número mayor es: " + mayor);
*/

/* Diseñe una función que recibe un valor numerico  que indica la cantidad de elementos del arreglo
El arreglo debera de llenarse con valores aleatorias en rango de 0 al 1000
posteriormente mostrara el arreglo */

// Capturamos un número
//const tamArreglo = prompt("Capture el tamaño del arreglo: ");


// **************************************************************************************************************
// Codigo de la Practica de Generador de Números

// Declaramos el Boton Generar como constante
const btngenerar = document.getElementById('btngenerar');

// Hacemos Global el Arreglo
var array = []; //Arreglo Vacio

// Codificamos el botón de Generar
btngenerar.addEventListener('click', function () {

    // COMBOBOX Generar
    const tamArreglo = document.getElementById('txtNumGen').value;
    // Llamamos la funcion
    numRandom(tamArreglo);

    // Promedio
    const lblprom = document.getElementById('promedio');
    const prom = promArreglo();
    lblprom.textContent = `El Promedio del Arreglo es: ${prom}`;

    // Numero Mayor
    const lblNumMay = document.getElementById('numMayor');
    const numMay = numMayor();
    lblNumMay.textContent = `El Mayor es ${numMay.numero}, en la Posición ${numMay.posicion}`;

    // Numero Menor
    const lblNumMen = document.getElementById('numMenor');
    const numMen = numMenor();
    lblNumMen.textContent = `El Menor es ${numMen.numero}, en la Posición ${numMen.posicion}`;

    // SIMETRIA
    const lblSimetria = document.getElementById('simetrico');
    const sime = simetrico(array);
    if(sime == true){
        lblSimetria.textContent = `Si es Simetrico`;
    }else if(sime == false){
        lblSimetria.textContent = `No es Simetrico`;
    }

    // PORCENTAJE DE PARES
    const lblPorPares = document.getElementById('porcPares');
    const porPares = pares(array);
    lblPorPares.textContent = `El Porcentaje de Pares es: ${porPares}`;

    // PORCENTAJE DE IMPARES
    const lblPorImpar = document.getElementById('porcImp');
    const porImp = impares(array);
    lblPorImpar.textContent = `El Porcentaje de Impares es: ${porImp}`;


});

// FUNCION PARA HACER LOS NUMEROS RANDOMS
function numRandom(tamArreglo) {

    const cmbRandoms = document.getElementById('cmbNumeros');

    for (con = 0; con < tamArreglo; ++con) {
        array[con] = Math.floor(Math.random() * 1000);

        // Construimos las opciones de manera dinamica
        let option = document.createElement('option');
        option.value = array[con];

        option.innerHTML = array[con];

        cmbRandoms.appendChild(option);
    }
}

// FUNCION PARA SACAR PROMEDIO
function promArreglo() {

    let prom = 0;

    for (con = 0; con < array.length; ++con) {
        prom = prom + array[con];
    }
    prom = prom / array.length;

    return prom;
}

// FUNCION PARA SACAR EL NUMERO MAYOR
function numMayor() {
    let mayor = 0;
    let posicion = 0;
    for (con = 0; con < array.length; ++con) {

        if (mayor <= array[con]) {
            mayor = array[con];
            posicion = con;
        } else {
            console.log("No es mayor que el anterior");
        }
    }
    posicion = posicion + 1;
    return { numero: mayor, posicion: posicion };
}

// FUNCION PARA SACAR EL NUMERO MENOR
function numMenor() {
    if (array.length === 0) {
        return { numero: null, posicion: null };
    }
    let menor = array[0];
    let posicion = 0;

    for (con = 0; con < array.length; ++con) {

        if (array[con] < menor) {
            menor = array[con];
            posicion = con;
        } else {
            console.log("Es mayor que el anterior");            
        }
    }
    posicion = posicion + 1;
    return { numero: menor, posicion: posicion };
}
// FUNCION PARA SIMETRIS
function simetrico(array) {
    if (!Array.isArray(array)) {
        throw new Error("El argumento no es un arreglo.");
    }
    var si = "SI";
    var no = "NO";
    const longitud = array.length;
    for (let i = 0; i < longitud / 2; i++) {
        if (array[i] !== array[longitud - 1 - i]) {
            return false;
        }
    }
    return true;
}

// FUNCION PARA SACAR LA SIMETRIA DE LOS PARES 
function pares(array) {
    if (!Array.isArray(array)) {
        throw new Error("El argumento no es un arreglo.");
    }
    if (array.length === 0) {
        throw new Error("El arreglo está vacío.");
    }
    let numerosPares = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            numerosPares++;
        }
    }
    const porcentaje = (numerosPares / array.length) * 100;
    return porcentaje;
}

// FUNCION PARA SACAR LA SIMETRIA DE LOS IMPARES 
function impares(array) {
    if (!Array.isArray(array)) {
        throw new Error("El argumento no es un arreglo.");
    }
    if (array.length === 0) {
        throw new Error("El arreglo está vacío.");
    }
    let numerosImpares = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 !== 0) {
            numerosImpares++;
        }
    }
    const porcentaje = (numerosImpares / array.length) * 100;
    return porcentaje;
}



