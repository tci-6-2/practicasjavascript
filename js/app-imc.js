// OBTENER EL BOTON CALCULAR
const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');

// CODIFICAMOS EL BOTON PARA CALCULAR EL IMC
btnCalcular.addEventListener('click', function () {
    let altura = parseFloat(document.getElementById('txtAltura').value);
    let peso = parseFloat(document.getElementById('txtPeso').value);

    // HACEMOS LOS CALCULOS
    let imc = (peso) / (altura * altura);

    // MOSTRAMOS LOS DATOS
    document.getElementById('txtResultado').value = imc.toFixed(2);

    // LLAMAMOS LA FUNCION DEL SEXO CON IMAGEN
    genImg(imc);
    calorias(peso);
});

// CODIFICAMOS EL BOTON DE LIMPIAR
btnLimpiar.addEventListener('click', function () {
    document.getElementById('txtAltura').value = '';
    document.getElementById('txtPeso').value = '';
    document.getElementById('txtResultado').value = '';
    document.getElementById('txtEdad').value = '';
    document.getElementById('txtCalorias').value = '';
});

// FUNCION CARGAR IMAGEN
function genImg(imc) {
    let img = document.getElementById('imagen');
    let GenH = document.getElementById('genH').checked;
    let GenM = document.getElementById('genM').checked;

    // HOMBRE
    if (GenH) {
        if (imc < 18.55) {
            img.src = "../img/01H.png";
        }
        else if (imc > 18.55 && imc <= 25) {
            img.src = "../img/02H.png";
        }
        else if (imc > 25 && imc <= 29.99) {
            img.src = "../img/03H.png";
        }
        else if (imc > 29.99 && imc <= 35) {
            img.src = "../img/04H.png";
        }
        else if (imc > 35 && imc <= 40) {
            img.src = "../img/05H.png";
        }
        else if (imc > 40) {
            img.src = "../img/06H.png";
        }
    }    

    // MUJER
    if (GenM) {
        if (imc < 18.55) {
            img.src = "../img/01M.png";
        }
        else if (imc > 18.55 && imc <= 25) {
            img.src = "../img/02M.png";
        }
        else if (imc > 25 && imc <= 29.99) {
            img.src = "../img/03M.png";
        }
        else if (imc > 29.99 && imc <= 35) {
            img.src = "../img/04M.png";
        }
        else if (imc > 35 && imc <= 40) {
            img.src = "../img/05M.png";
        }
        else if (imc > 40) {
            img.src = "../img/06M.png";
        }
    }
    
}

function calorias(peso) {
    let edad = document.getElementById('txtEdad').value;
    let calorias = 0;
    let GenH = document.getElementById('genH').checked;
    let GenM = document.getElementById('genM').checked;

    if (GenH) {
        if (edad < 18) {
            calorias = (17.686 * peso) + 692.2;
        }
        else if (edad > 18 && edad <= 30) {
            calorias = (15.057 * peso) + 692.2;
        }
        else if (edad > 30 && edad <= 60) {
            calorias = (11.472 * peso) + 873.1;
        }
        else if (edad > 60) {
            calorias = (11.711 * peso) + 587.7;
        }
    }
    else if (GenM) {
        if (edad < 18) {
            calorias = (13.384 * peso) + 692.6;
        }
        else if (edad > 18 && edad <= 30) {
            calorias = (14.818 * peso) + 486.6;
        }
        else if (edad > 30 && edad <= 60) {
            calorias = (8.126 * peso) + 845.6;
        }
        else if (edad > 60) {
            calorias = (9.082 * peso) + 658.5;
        }
    }
    

    document.getElementById('txtCalorias').value = calorias.toFixed(2)
}

function deseleccionarRadio() {
    var generos = document.getElementsByName('Genero');

    for (var i = 0; i < generos.length; i++) {
        generos[i].checked = false;
    }
}

