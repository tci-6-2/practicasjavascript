// OBTENER EL OBJETO BUTTON DE CALCULAR
const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById('btnLimpiar');

btnCalcular.addEventListener('click', function () {
    let valorAuto = parseFloat(document.getElementById('txtValorAuto').value);
    let porcentaje = parseFloat(document.getElementById('txtPorcentaje').value);
    let plazo = parseFloat(document.getElementById('plazos').value);

    // HACER LOS CALCULOS
    let pagoInicial = valorAuto * (porcentaje / 100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin / plazo;

    // MOSTRAR LOS DATOS
    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFiin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;
});

// CODIFICAR EL BOTON DE LIMPIAR
btnLimpiar.addEventListener('click', function () {
    // Restablecer los campos de texto a sus valores iniciales
    document.getElementById('txtValorAuto').value = '';
    document.getElementById('txtPorcentaje').value = '';
    document.getElementById('plazos').value = '';
    document.getElementById('txtPagoInicial').value = '';
    document.getElementById('txtTotalFiin').value = '';
    document.getElementById('txtPagoMensual').value = '';
});